module.exports = {
    preset: 'react-native',
    setupFiles: ['<rootDir>/mock/index.js'],
    transformIgnorePatterns: [
      "node_modules/(?!(jest-)?@?react-native|@react-native-community|@react-navigation|react-navigation-shared-element|react-navigation|@react-native-firebase)"
    ],
};