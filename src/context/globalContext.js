import React, {createContext, useReducer, useContext} from 'react';
import {TEST_ACTION, UPDATE_USER} from './ActionType';
const GlobalStateContext = createContext();
const GlobalDispatchContext = createContext();
const globalReducer = (state, action) => {
  switch (action.type) {
    case TEST_ACTION: {
      return {
        ...state,
        test: 2,
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        user: action.user,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

//Provider
export const GlobalProvider = ({children}) => {
  const [state, dispatch] = useReducer(globalReducer, {
    test: 1,
    user: null,
  });

  return (
    <GlobalDispatchContext.Provider value={dispatch}>
      <GlobalStateContext.Provider value={state}>
        {children}
      </GlobalStateContext.Provider>
    </GlobalDispatchContext.Provider>
  );
};

//custom hooks for when we want to use our global state
export const useGlobalStateContext = () => useContext(GlobalStateContext);

export const useGlobalDispatchContext = () => useContext(GlobalDispatchContext);
