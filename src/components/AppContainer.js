import React, {useEffect, useState} from 'react';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import {createAppContainer} from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import {
  useGlobalDispatchContext,
  useGlobalStateContext,
} from '../context/globalContext';
import auth from '@react-native-firebase/auth';
import {UPDATE_USER} from '../context/ActionType';
const notAuthStackNavigator = createSharedElementStackNavigator(
  {
    Login: LoginScreen,
    SignUp: SignUpScreen,
  },
  {
    initialRouteName: 'Login',
    // defaultNavigationOptions: {
    //   headerShown: false,
    // },
  },
);
const authStackNavigator = createSharedElementStackNavigator(
  {
    Home: HomeScreen,
    // Login: LoginScreen,
    // SignUp: SignUpScreen,
  },
  {
    initialRouteName: 'Home',
  },
);
const NotAuthAppNavigator = createAppContainer(notAuthStackNavigator);
const AuthAppNavigator = createAppContainer(authStackNavigator);
const AppContainer = () => {
  const [initializing, setInitializing] = useState(true);
  const {user} = useGlobalStateContext();
  const dispatch = useGlobalDispatchContext();
  useEffect(() => {
    console.log('initializing', initializing);
  }, [initializing]);
  //   const onAuthStateChanged = ;
  useEffect(() => {
    try {
      const subscriber = auth().onAuthStateChanged(authUser => {
        console.log(authUser);
        dispatch({type: UPDATE_USER, user: authUser});
        if (initializing) {
          setInitializing(false);
        }
      });
    } catch (error) {
      console.log(error);
    }
    // return subscriber; // unsubscribe on unmount
  }, []);
  // console.log(initializing);
  if (initializing) {
    return <></>;
  }

  if (user) {
    return <AuthAppNavigator />;
  }
  return <NotAuthAppNavigator />;
};
export default AppContainer;
