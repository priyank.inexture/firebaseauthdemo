import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
const Button = ({title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
      <View style={styles.container}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#01ba76',
    borderRadius: 6,
    paddingVertical: 10,
  },
  text: {
    fontWeight: 'normal',
    textAlign: 'center',
    fontSize: 16.5,
    color: '#fff',
  },
});
export default Button;
