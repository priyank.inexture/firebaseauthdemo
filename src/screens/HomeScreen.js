import React from 'react';
import {View, Text, Button} from 'react-native';
import {useGlobalStateContext} from '../context/globalContext';
import auth from '@react-native-firebase/auth';
const HomeScreen = () => {
  const {user} = useGlobalStateContext();
  const handleSignOut = () => {
    auth()
      .signOut()
      .then(() => console.log('User signed out!'));
  };
  return (
    <View>
      <Text>Home screen {user.email}</Text>
      <Button title="Sign Out" onPress={handleSignOut} />
    </View>
  );
};
export default HomeScreen;
