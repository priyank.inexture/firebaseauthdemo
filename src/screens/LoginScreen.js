import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  // Button,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Button from '../components/Button';
import auth from '@react-native-firebase/auth';
import {useGlobalDispatchContext} from '../context/globalContext';
import {UPDATE_USER} from '../context/ActionType';
const {width, height} = Dimensions.get('window');
const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useGlobalDispatchContext();

  const handleSignUpNow = () => {
    navigation.navigate('SignUp');
  };
  const handleLogin = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(userCredential => {
        dispatch({type: UPDATE_USER, user: userCredential.user});
        console.log('User account signed in!');
      })
      .catch(error => {
        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  };
  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Welcome Back</Text>
      <TextInput onChangeText={value => setEmail(value)} placeholder="Email" />
      <TextInput
        onChangeText={value => setPassword(value)}
        placeholder="Password"
        secureTextEntry
      />
      <Button title="LOGIN" onPress={handleLogin} />
      <View style={styles.alignCenter}>
        <Text style={styles.signUp}>
          Don't have account?{' '}
          <Text style={styles.link} onPress={handleSignUpNow}>
            create a new account
          </Text>
        </Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
    padding: width * 0.1,
    // alignItems: 'center',
  },
  heading: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 21,
  },
  link: {
    color: '#00a060',
  },
  alignCenter: {
    alignItems: 'center',
  },
  signUp: {
    marginTop: height * 0.03,
  },
});
LoginScreen.navigationOptions = () => {
  return {
    headerShown: false,
  };
};
export default LoginScreen;
