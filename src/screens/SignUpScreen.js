import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, Dimensions, StyleSheet} from 'react-native';
import auth from '@react-native-firebase/auth';
import Button from '../components/Button';
const {width, height} = Dimensions.get('window');

const SignUpScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const handleSignUp = () => {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        console.log('User account created & signed in!');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  };
  return (
    <View style={styles.container}>
      <Text style={styles.heading}>Create Account</Text>
      <TextInput onChangeText={value => setEmail(value)} placeholder="Email" />
      <TextInput
        onChangeText={value => setPassword(value)}
        placeholder="Password"
      />
      <Button title="Sign Up" onPress={handleSignUp} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: width * 0.1,
  },
  heading: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 21,
  },
});
SignUpScreen.navigationOptions = () => {
  return {
    headerBackground: () => null,
    headerTitle: () => <Text />,
  };
};
export default SignUpScreen;
