/**
 * @format
 */
// Note: test renderer must be required after react-native.
import * as React from 'react';
import * as renderer from 'react-test-renderer';
import {ProviderWrapper} from '../';

it('renders correctly', () => {
  renderer.create(<ProviderWrapper />);
});
