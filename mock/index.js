import 'react-native-gesture-handler/jestSetup';

jest.mock('react-native-reanimated', () => {
  const Reanimated = require('react-native-reanimated/mock');

  // The mock for `call` immediately calls the callback which is incorrect
  // So we override it with a no-op
  Reanimated.default.call = () => {};

  return Reanimated;
});

// Silence the warning: Animated: `useNativeDriver` is not supported because the native animated module is missing
// jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

// As of react-native@0.64.X file has moved
jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');

jest.mock('@react-native-firebase/auth', () => {
  return {
    initializeApp: jest.fn(() => {
      return {
        auth: jest.fn(() => {
          return {
            createUserWithEmailAndPassword: jest.fn((para1, para2) => {
              return new Promise(function (resolve, reject) {
                resolve({
                  email: 'user1@gmail.com',
                  uid: '12345678abcdefg',
                });

                reject({message: 'error!'});
              });
            }),
            signOut: jest.fn(() => {
              return new Promise(function (resolve, reject) {
                resolve('Success');
                reject({message: 'error!'});
              });
            }),
            onAuthStateChanged: jest.fn(callback => {
              callback({
                email: 'user1@gmail.com',
                uid: '12345678abcdefg',
              });
              // return ;
            }),
            signInWithEmailAndPassword: jest.fn((para1, para2) => {
              return new Promise(function (resolve, reject) {
                reject({message: 'error!'});
              });
            }),
          };
        }),
      };
    }),
  };
});
